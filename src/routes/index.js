const express = require("express");
const router = express.Router();

/** GET default page. */
router.get("/", function (req, res) {
  res.send("API root");
});

module.exports = router;
