const express = require("express");
const cors = require("cors");
const config = require("./config");
const router = require("./src/routes");
const userRouter = require("./src/routes/user");
const authRouter = require("./src/routes/auth");

//Create express app
const app = express();

//Middlewares
app.use(express.json());
app.use(cors());
const http = require("http").createServer(app);

app.use("/", router);
app.use("/user", userRouter);
app.use("/auth", authRouter());

//Listen to the port
http.listen(config.PORT, () => {
  console.log(`listening on port :${config.PORT}`);
});
